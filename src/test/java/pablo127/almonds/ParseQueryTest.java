package pablo127.almonds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public class ParseQueryTest {

	@Before
	public void setUp() throws Exception {
		Parse.initialize("B3jrmh2W9yIzYCjOLHLI5tLcApvFl1NJAoiJMnTP", "eHDcUJ9DJJqrIsccHbBUFjb0d3srvtnbAJoQxhLL");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindTestValue() {
		ParseQuery query = new ParseQuery("test");
		List<ParseObject> objects = null;
		try {
			objects = query.find();
		} catch (ParseException e) {
			assertEquals(null, e);
		}
		assertNotEquals(null, objects);
		assertEquals(1, objects.size());
		assertEquals("hhh", objects.get(0).getString("x"));
	}
	
	@Test
	public void testHandlingDate(){
		ParseQuery q = new ParseQuery("daty");
		q.find(new FindCallback() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				assertEquals(null, e);
				assertTrue(objects.size() > 0);
				for(int i = 0; i < objects.size(); i++){
					//System.out.println("obj "+objects.get(i).get("date"));
					Calendar cal = objects.get(i).getCalendarDate("date");	
					//System.out.println("X: year " + cal.get(Calendar.YEAR) + " mon " + cal.get(Calendar.MONTH)  );
					//System.out.println(" "+cal.getTime());
				}
			}
		});
	}
	
	@Test
	public void testWhereEqualToDatesNewEntries(){
		final Calendar cal = Calendar.getInstance();
		List<ParseObject> toDelete = new ArrayList<ParseObject>();
		cal.set(2014, 11, 8, 13, 16);
		ParseObject obj = new ParseObject("daty");
		obj.put("date", cal);
		save(obj);
		toDelete.add(obj);
		obj = new ParseObject("daty");
		obj.put("date", cal);
		obj.put("mode", true);
		save(obj);
		toDelete.add(obj);
		obj = new ParseObject("daty");
		obj.put("date", cal);
		obj.put("mode", false);
		save(obj);
		toDelete.add(obj);
		
		HashMap<String, Object> whereEqualTo = new HashMap<String, Object>();
		whereEqualTo.put("date", cal);
		testSize(whereEqualTo, 3);
		whereEqualTo.put("mode", true);
		testSize(whereEqualTo, 1);
		whereEqualTo.put("mode", false);
		testSize(whereEqualTo, 1);
		
		for(ParseObject o: toDelete) {
			try {
				o.delete();
			} catch (ParseException e) {
				assertNull(e);
			}
		}
			
	}
	
	private void save(ParseObject obj){
		obj.save(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				assertEquals(null, e);
			}
		});
	}
	
	@Test
	public void testWhereEqualToWithDates(){
		ParseQuery q = new ParseQuery("daty");
		q.find(new FindCallback() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				assertEquals(null, e);
				assertTrue(objects.size() > 0);
				for(int i = 0; i < objects.size(); i++){
					//System.out.println("data "+objects.get(i).getDate("date"));
					testSize("date", objects.get(i).getCalendarDate("date"), 1);
					testSize("date", objects.get(i).getDate("date"), 1);
				}
					
			}
		});		
	}
	
	@Test
	public void testWhereEqualToQuestions(){
		testSize("mode", true, 8);
		testSize("mode", false, 2);
		testSize("word", "a", 4);
		testSize("word", "b", 4);
		testSize("word", "c", 2);
		
		HashMap<String, Object> whereEqualTo = new HashMap<String, Object>();
		testSize(whereEqualTo, 10);
		whereEqualTo.put("word", "a");
		whereEqualTo.put("mode", true);
		testSize(whereEqualTo, 3);
		whereEqualTo.put("mode", false);
		testSize(whereEqualTo, 1);
	}
	
	private void testSize(String key, Object val, int size){
		HashMap<String, Object> whereEqualTo = new HashMap<String, Object>();
		whereEqualTo.put(key, val);		
		testSize(whereEqualTo, size);
	}
	
	private void testSize(HashMap<String, Object> whereEqualTo, final int size){
		ParseQuery h = new ParseQuery("daty");
		Iterator<Entry<String, Object>> it = whereEqualTo.entrySet().iterator();
		while(it.hasNext()){
			Entry<String, Object> en = it.next();
			h.whereEqualTo(en.getKey(), en.getValue());
		}
		h.find(new FindCallback() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				assertEquals(null, e);
				assertNotEquals(null, objects);
				assertTrue("objects size: "+objects.size()+", should be: "+size, objects.size() == size);
			}
		});
	}

}
