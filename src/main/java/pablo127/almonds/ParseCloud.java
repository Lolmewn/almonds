package pablo127.almonds;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public class ParseCloud {
	
	public static void callFunctionInBackground(final String nameOfFunction, final HashMap<String, Object> params, 
			final FunctionCallback<String> functionCallback){
		
		new Thread(new Runnable() {
			
			public void run() {
				String result = null;
				ParseException e = null;
				try	{
					result = callFunction(nameOfFunction, params);
				} catch (ParseException e1) {
					e = e1;
				}
				
				functionCallback.done(result, e);
				
			}

			private String callFunction(String nameOfFunction, HashMap<String, Object> params) throws ParseException  {
				try	{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost(Parse.getParseAPIUrlFunctions() + nameOfFunction);
					httpPost.addHeader("X-Parse-Application-Id", Parse.getApplicationId());
					httpPost.addHeader("X-Parse-REST-API-Key", Parse.getRestAPIKey());
					httpPost.addHeader("Content-Type", "application/json");
													
					httpPost.setEntity(new StringEntity(this.getParams(params)));										
					
					Header[] headers = httpPost.getAllHeaders();
					HttpResponse httpResponse = httpclient.execute(httpPost);

					ParseResponse parseResponse = new ParseResponse(httpResponse);
					
					
					if (parseResponse.isFailed()) {
						throw parseResponse.getException();
					}
					
				
					JSONObject obj = parseResponse.getJsonObject();

					if (obj == null) {
						throw parseResponse.getException();
					}
					try{
						if(obj.get("error") != null){
							throw new ParseException(Integer.parseInt(obj.get("code").toString()), "error: "+obj.get("error"));
						}
					}catch (JSONException e)	{

					}
					
					return obj.get("result").toString();				
				}
				catch (ClientProtocolException e){
					throw ParseResponse.getConnectionFailedException(e);
				}catch (IOException e){
					throw ParseResponse.getConnectionFailedException(e);
				}catch (JSONException e)	{
					e.printStackTrace();
				}
				return null;
			}

			private String getParams(HashMap<String, Object> params){
				if(params == null || params.size() == 0)
					return "{}";
				String wynik = "{";
				Iterator<Entry<String, Object>> i = params.entrySet().iterator();
				while(i.hasNext()){
					if(wynik.length() > 1)
						wynik += ",";
					Entry<String, Object> pair = (Entry<String, Object>) i.next();
					wynik += "\"" + pair.getKey() + "\": \"" + String.valueOf(pair.getValue()) + "\"";
				}
				return wynik + "}";
			}
		}).start();
		
		
	}


}
