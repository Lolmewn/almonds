package pablo127.almonds;
/**
 * 
 * @author jskrepnek (<a href="https://bitbucket.org/jskrepnek" target="_blank">bitbucket page</a>)
 */
public abstract class DeleteCallback {
	public abstract void done(ParseException e);	
}
