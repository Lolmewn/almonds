package pablo127.almonds;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public interface SignUpCallback {
	public void done(ParseException e);
}
