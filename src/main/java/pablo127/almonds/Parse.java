package pablo127.almonds;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 * @author jskrepnek (<a href="https://bitbucket.org/jskrepnek" target="_blank">bitbucket page</a>)
 */
public class Parse {
	private static String mApplicationId;
	private static String mRestAPIKey;

	private static final String PARSE_API_URL = "https://api.parse.com";
	private static final String PARSE_API_URL_CLASSES = "/1/classes/";
	private static final String PARSE_API_URL_USERS = "/1/users/";
	private static final String PARSE_API_URL_USERS_LOGIN = "/1/login";
	private static final String PARSE_API_URL_FUNCTIONS = "/1/functions/";

	/**
	 * @param applicationId
	 * @param restAPIKey
	 */
	public static void initialize(String applicationId, String restAPIKey) {
		mApplicationId = applicationId;
		mRestAPIKey = restAPIKey;
	}

	public static String getApplicationId() {
		return mApplicationId;
	}

	public static String getRestAPIKey() {
		return mRestAPIKey;
	}

	public static String getParseAPIUrl() {
		return PARSE_API_URL;
	}

	public static String getParseAPIUrlClasses() {
		return getParseAPIUrl() + PARSE_API_URL_CLASSES;
	}

	public static String getParseAPIUrlUsers() {
		return getParseAPIUrl() + PARSE_API_URL_USERS;
	}

	public static String getParseAPIUrlUsersDelete(){
		return getParseAPIUrlUsers() + "g7y9tkhB7";
	}

	public static String getParseAPIUrlLogin(){
		return getParseAPIUrl() + PARSE_API_URL_USERS_LOGIN;
	}

	public static String getParseAPIUrlSessionTokenValidation(){
		return getParseAPIUrlUsers() + "me";
	}

	public static String getParseAPIUrlFunctions() {
		return getParseAPIUrl() + PARSE_API_URL_FUNCTIONS;
	}	

}
